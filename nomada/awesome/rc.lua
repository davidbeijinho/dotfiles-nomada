-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")
require("lib/error_handler")

-- Standard awesome library
local awful = require("awful") --Everything related to window managment
require("awful.autofocus")

-- Widget and layout library
local wibox = require("wibox")


-----------------------------------------------------------------------------------------------
-- NOMADA CONFIG
awful.layout.layouts = {
    awful.layout.suit.max, 
    awful.layout.suit.tile, 
    awful.layout.suit.fair,
    awful.layout.suit.floating, 
    awful.layout.suit.max.fullscreen
    -- awful.layout.suit.tile.left,
    -- awful.layout.suit.tile.bottom,
    -- awful.layout.suit.tile.top,
    -- awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.spiral,
    -- awful.layout.suit.spiral.dwindle,
    -- awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}

-- Import Keybinds
local keys = require("keys/keys")
root.keys(keys.global_keys)

local buttons = require("buttons/buttons")
root.buttons(buttons.global_buttons)

-- Import rules
local rules = require("lib/rules")
awful.rules.rules = rules

-- Import signals
local signals =require("lib/signals")
signals.init();

-- Import main menu
local mymainmenu = require("widgets/mainmenu")
awful.util.mymainmenu= mymainmenu

-- Import theme handler
require("lib/theme")

local volume = require("widgets/volume")
local lock = require('widgets/lock')
local battery = require("widgets/battery")
local brightness = require("widgets/brightness")
local cpu = require("widgets/cpu")
local ram = require("widgets/ram")

-- Auto Run apps
-- require("autoRun")
-----------------------------------------------------------------------------------------------

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    signals.set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag(
        {"1-www", "2-term", "3-code", "4-files", "5-notes", "6", "7", "8", "9"},
        s,
        awful.layout.layouts[1]
    )

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)

    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = buttons.taglist_buttons 
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = buttons.tasklist_buttons
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        -- Left widgets
        { 
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist,
            s.mypromptbox,
        },
        -- Middle widget
        s.mytasklist,
        -- Right widgets
        {
            layout = wibox.layout.fixed.horizontal,
            cpu,
            volume,
            lock,
            battery,
            brightness,
            ram,
            wibox.widget.systray(),
            wibox.widget.textclock(),
            s.mylayoutbox,
        },
    }
end
)
