local global_buttons = require("buttons/global_buttons")
local tasklist_buttons = require("buttons/tasklist_buttons")
local client_buttons = require("buttons/client_buttons")
local taglist_buttons = require("buttons/taglist_buttons")

return {
    global_buttons = global_buttons,
    tasklist_buttons = tasklist_buttons,
    client_buttons = client_buttons,
    taglist_buttons = taglist_buttons
}