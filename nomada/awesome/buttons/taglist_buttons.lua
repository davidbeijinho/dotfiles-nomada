local gears = require("gears")
local awful = require("awful")

local constants = require("lib/constants")

return  gears.table.join(
    awful.button(
        {},
        constants.buttons.left,
        function(t)
            t:view_only() 
        end
    ),
    awful.button(
        {},
        constants.buttons.scroll_up,
        function(t)
            awful.tag.viewnext(t.screen)
        end
    ), 
    awful.button(
        {},
        constants.buttons.scroll_down,
        function(t) 
            awful.tag.viewprev(t.screen) 
        end
    )
)

-- awful.button(
--     {},
--     constants.buttons.right,
--     awful.tag.viewtoggle(t)
-- ),
-- awful.button(
--     { 
--         client.keys.modkey 
--     },
--     constants.buttons.left,
--     function(t)
--         if client.focus then
--             client.focus:move_to_tag(t)
--         end
--     end
-- ),
-- awful.button(
--     { 
--         modkey 
--     }, 
--     1, 
--     function(t)
--         if client.focus then
--             client.focus:move_to_tag(t)
--         end
--     end
-- ),
-- awful.button(
--     { 
--     modkey
--     },
--     3, 
--     function(t)
--         if client.focus then
--             client.focus:toggle_tag(t)
--         end
--     end
-- ),
