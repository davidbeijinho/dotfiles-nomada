
local gears = require("gears")
local awful = require("awful")

local constants = require("lib/constants")

return  gears.table.join( 
    awful.button(
        {}, 
        constants.buttons.left, 
        function(c)
            if c == client.focus then
                c.minimized = true
            else
                c:emit_signal("request::activate", "tasklist", {raise = true})
            end
        end
    )
)
