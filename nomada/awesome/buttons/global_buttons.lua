local gears = require("gears")
local awful = require("awful")

local constants = require("lib/constants")

return gears.table.join(
    awful.button(
        {},
        constants.buttons.right,
        function()
            awful.util.mymainmenu:toggle()
        end
    ), 
    awful.button(
        {}, 
        constants.buttons.scroll_up, 
        function()
            awful.tag.viewnext()
        end
    ),
    awful.button(
        {}, 
        constants.buttons.scroll_down,
        function()
            awful.tag.viewprev()
        end
    )
)
