local gears = require("gears")
local awful = require("awful")

local constants = require("lib/constants")

return gears.table.join(
    awful.button(
        {}, 
        constants.buttons.left,
        function(c)
            c:emit_signal("request::activate", "mouse_click", {raise = true})
        end
    ), 
    awful.button(
        {
            constants.keys.mod
        }, 
        constants.buttons.left,
        function(c)
            c:emit_signal("request::activate", "mouse_click", {raise = true})
            awful.mouse.client.move(c)
        end
    ), 
    awful.button(
        {
            constants.keys.mod
        }, 
        constants.buttons.right,
        function(c)
            c:emit_signal("request::activate", "mouse_click", {raise = true})
            awful.mouse.client.resize(c)
        end
    )
)

