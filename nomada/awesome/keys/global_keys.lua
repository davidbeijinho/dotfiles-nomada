local gears = require("gears")
local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup")
local menubar = require("menubar")

local constants = require("lib/constants")
local xrandr = require("lib/xrandr")

local volume = require("widgets/volume")

return gears.table.join(
    ---------------------------------------------------
    --  FN KEYS
    ---------------------------------------------------
    awful.key(
        {},
        "XF86AudioRaiseVolume",
        function ()
            os.execute("pactl set-sink-volume @DEFAULT_SINK@ +10%")
        end,
        {
            description = "Volume Raise",
            group = "FN Keys"
        }
    ),
    awful.key(
        {},
        "XF86AudioLowerVolume",
        function ()
            os.execute("pactl set-sink-volume @DEFAULT_SINK@ -10%")
        end,
        {
            description = "Volume Lower",
            group = "FN Keys"
        }
    ),
    awful.key(
        {},
        "XF86AudioMute",
        function ()
            os.execute("pactl set-sink-mute @DEFAULT_SINK@ toggle")
        end,
        {
            description = "Volume Mute",
            group = "FN Keys"
        }
    ),
    awful.key(
        {}, 
        "XF86MonBrightnessUp", 
        function () 
            os.execute("xbacklight -inc 10") 
        end,
        {
            description = "Brightness up", 
            group = "FN Keys"
        }
    ),
    awful.key(
        {},
        "XF86MonBrightnessDown",
        function () 
            os.execute("xbacklight -dec 10") 
        end,
        {
           description = "Brightness down", 
           group = "FN Keys"
        }
    ),
    ---------------------------------------------------
    --  APPS KEYS
    ---------------------------------------------------
    awful.key(
        {
            constants.keys.alt
        },
        "c", 
        function()
            awful.spawn(constants.apps.editor)
        end,
        {
            description = "open a editor", 
            group = "Apps"
        }
    ),
    awful.key(
        {
            constants.keys.alt
        },
        "b",
        function()
            awful.spawn(constants.apps.browser) 
        end,
        {
            description = "open a browser", 
            group = "Apps"
        }
    ),
    awful.key(
        {
            constants.keys.alt
        },
        "Return",
        function()
            awful.spawn(constants.apps.terminal) 
        end,
        {
            description = "open a terminal", 
            group = "Apps"
        }
    ),
    -- File Manager
    awful.key(
        {
            constants.keys.alt
        },
        "f", 
        function()
            awful.spawn(constants.apps.fileManager)
        end,
        {
            description = "open a file manager", 
            group = "Apps"
        }
    ),
    awful.key(
        {
            constants.keys.mod
        },
        "v",
        function()
            awful.util.spawn("pavucontrol") 
        end,
        {
            description = "pulseaudio control", 
            group = "Apps"
        }
    ),    
    ---------------------------------------------------
    --  ACTIONS KEYS
    ---------------------------------------------------    
    awful.key(
        {
            constants.keys.mod
        }, 
        "Print", 
        function()
            awful.util
            .spawn('xfce4-screenshooter -r --save ./Pictures/print-' .. os.date("%Y-%m-%d-%H:%M:%S") .. '.jpg')
        end,
        {
            description = "Screenshot area", 
            group = "screenshots"
        }
    ),
    awful.key(
        {}, 
        "Print",
        function()
            awful.util
            .spawn('xfce4-screenshooter -f --save ./Pictures/print-' .. os.date("%Y-%m-%d-%H:%M:%S") .. '.jpg')
        end,
        {
            description = "Screenshot fullscren", 
            group = "screenshots"
        }
    ),
    awful.key(
        {
            constants.keys.mod, 
            constants.keys.control
        },
        "l",
        function()
            awful.util.spawn("slock")
        end,
        {
            description = "screen lock",
            group = "system"
        }
    ),
    awful.key(
        {
            constants.keys.mod
        }, 
        "Escape",
        function()
            awful.util.spawn("xkill") 
        end,
        {
            description = "Kill proces",
            group = "system"
        }
    ),
    awful.key(
        {
            constants.keys.mod
        },
        "d",
        function()
            xrandr.xrandr() 
        end,
        {
            description = "change displays config", 
            group = "system"
        }
    ),
    ---------------------------------------------------
    --  AWESOME KEYS
    ---------------------------------------------------      
    awful.key(
        {
            constants.keys.mod
        }, 
        "s",
        function()
            hotkeys_popup.show_help()
        end,
        {
            description = "show help", 
            group = "awesome"
        }
    ),
    awful.key(
        {
            constants.keys.mod, 
            constants.keys.control
        }, 
        "r",
        function()
            awesome.restart() 
        end,
        {
            description = "reload awesome",
            group = "awesome"
        }
    ), 
    awful.key(
        {
            constants.keys.mod,
            constants.keys.shift
        },
        "q",
        function()
            awesome.quit()
        end,
        {
            description = "quit awesome",
            group = "awesome"
        }
    ),
    ---------------------------------------------------
    --  TAGS KEYS
    ---------------------------------------------------      
    awful.key(
        {
            constants.keys.mod
        },
        "Left",
        function()
            awful.tag.viewprev()
        end,
        {
            description = "view previous", 
            group = "tag"
        }
    ),
    awful.key(
        {
            constants.keys.mod
        }, 
        "Right",
        function()
            awful.tag.viewnext()
        end,
        {
            description = "view next", 
            group = "tag"
        }
    ),
    awful.key(
        {
            constants.keys.mod
        },
        "Escape",
        function()
            awful.tag.history.restore()
        end,
        {
            description = "go back", 
            group = "tag"
        }
    ),
    ---------------------------------------------------
    --  CLIENT KEYS
    ---------------------------------------------------     
    awful.key(
        {
            constants.keys.mod
        }, 
        "u",
        function()
            awful.client.urgent.jumpto()
        end,
        {
            description = "jump to urgent client",
            group = "client"
        }
    ),
    awful.key(
        {
            constants.keys.alt
        }, 
        constants.keys.tab,
        function()
            awful.client.focus.byidx(1) 
        end,
        {
            description = "focus next by index", 
            group = "client"
        }
    ),
    awful.key(
        {
            constants.keys.alt, 
            constants.keys.shift
        },
        constants.keys.tab,
        function()
            awful.client.focus.byidx(-1) 
        end,
        {
            description = "focus previous by index",
            group = "client"
        }
    ), 
    awful.key(
        {
            constants.keys.mod, 
            constants.keys.shift
        },
        "j",
        function()
            awful.client.swap.byidx(1) 
        end, 
        {
            description = "swap with next client by index",
            group = "client"
        }
    ),
    awful.key(
        {
            constants.keys.mod,
            constants.keys.shift
        },
        "k",
        function()
            awful.client.swap.byidx(-1) 
        end, 
        {
            description = "swap with previous client by index",
            group = "client"
        }
    ),
    awful.key(
        {
            constants.keys.mod
        },
        constants.keys.tab,
        function()
            awful.client.focus.history.previous()
                if client.focus then 
                    client.focus:raise() 
                end
        end, 
        {
            description = "go back",
            group = "client"
        }
    ),
    awful.key(
        {
            constants.keys.mod, 
            constants.keys.control
        },
        "n",
        function()
            local c = awful.client.restore()
            if c then
                c:emit_signal("request::activate", "key.unminimize", {raise = true})
            end
        end, 
        {
            description = "restore minimized", 
            group = "client"
        }
    ),    
    ---------------------------------------------------
    --  SCREEN KEYS
    ---------------------------------------------------
    awful.key(
        {
            constants.keys.mod
            , constants.keys.control
        }, 
        "j",
        function()
            awful.screen.focus_relative(1) 
        end,
        {
            description = "focus the next screen", 
            group = "screen"
        }
    ),
    awful.key(
        {
            constants.keys.mod,
            constants.keys.control
        },
        "k",
        function()
            awful.screen.focus_relative(-1) 
        end,
        {
            description = "focus the previous screen",
            group = "screen"
        }
    ),       
    ---------------------------------------------------
    --  LAYOUT MANIPULATION KEYS
    ---------------------------------------------------         
    awful.key(
        {
            constants.keys.mod
        }, 
        "l",
        function() 
            awful.tag.incmwfact(0.05) 
        end, 
        {
            description = "increase master width factor",
            group = "layout"
        }
    ),
    awful.key(
        {
            constants.keys.mod
        },
        "h",
        function()
            awful.tag.incmwfact(-0.05) 
        end, 
        {
            description = "decrease master width factor",
            group = "layout"
        }
    ),
    awful.key(
        {
            constants.keys.mod, 
            constants.keys.shift
        },
        "h",
        function()
            awful.tag.incnmaster(1, nil, true) 
        end, 
        {
            description = "increase the number of master clients",
            group = "layout"
        }
    ),
    awful.key(
        {
            constants.keys.mod, 
            constants.keys.shift
        }, 
        "l",
        function() 
            awful.tag.incnmaster(-1, nil, true) 
        end, 
        {
            description = "decrease the number of master clients",
            group = "layout"
        }
    ),
    awful.key(
        {
            constants.keys.mod,
            constants.keys.control
        }, 
        "h",
        function() 
            awful.tag.incncol(1, nil, true) 
        end,
        {
            description = "increase the number of columns",
            group = "layout"
        }
    ),
    awful.key(
        {
            constants.keys.mod,
            constants.keys.control
        },
        "l",
        function()
            awful.tag.incncol(-1, nil, true) 
        end, 
        {
            description = "decrease the number of columns",
            group = "layout"
        }
    ), 
    ---------------------------------------------------
    --  LAYOUT KEYS
    ---------------------------------------------------      
    awful.key(
        {
            constants.keys.mod
        },
        "space",
        function() 
            awful.layout.inc(1) 
        end,
        {
            description = "select next", 
            group = "layout"
        }   
    ),
    awful.key(
        {
            constants.keys.mod, 
            constants.keys.shift
        },
        "space",
        function()
            awful.layout.inc(-1) 
        end, 
        {
            description = "select previous",
            group = "layout"
        }
    ),
    ---------------------------------------------------
    --  MENU KEYS
    ---------------------------------------------------       
    awful.key(
        {
            constants.keys.mod
        }, 
        "p",
        function()
            menubar.show() 
        end,
        {
            description = "show the menubar",
            group = "launcher"
        }
    ),
    awful.key(
        {
            constants.keys.mod
        },
        "r",
        function() 
            awful.screen.focused().mypromptbox:run() 
        end,
        {
            description = "run prompt", 
            group = "launcher"
        }
    )
    ---------------------------------------------------
    --  DISABLED KEYS
    ---------------------------------------------------         
    -- awful.key(
    --     { 
    --         constants.keys.control, 
    --         altkey   
    --     }, 
    --     "o", 
    --     function() 
    --         awful.spawn.with_shell("$HOME/.config/awesome/scripts/picom-toggle.sh") 
    --     end,
    --     {
    --         description = "Picom toggle", 
    --         group = "alt+ctrl"
    --     }
    -- ),
    -- awful.key(
    --     {
    --         constants.keys.mod,
    --         constants.keys.control
    --     },
    --     "s", 
    --     function()
    --         awful.util.spawn("catfish") 
    --     end,
    --     {
    --         description = "catfish", 
    --         group = "mod+ctrl"
    --     }
    -- ),
    -- awful.key(
    --     {
    --         constants.keys.mod
    --     },
    --     "l",
    --     function()
    --         awful.util.spawn("rofi -show run -fullscreen")
    --     end,
    --     {
    --         description = "rofi fullscreen",
    --         group = "function keys"
    --     }
    -- ),
    -- awful.key(
    --     {
    --         constants.keys.mod
    --     }, 
    --     "o",
    --     function()
    --         awful.util.spawn("rofi -show run")
    --     end,
    --     {
    --         description = "rofi",
    --         group = "function keys"
    --     }
    -- ),
    -- awful.key(
    --     {
    --         constants.keys.mod
    --     },
    --     "ñ", -- TODO change this key
    --     function()
    --         awful.util.spawn("rofi-theme-selector")
    --     end,
    --     {
    --         description = "rofi theme selector",
    --         group = "super"
    --     }
    -- ),
    -- awful.key(
    --     {constants.keys.alt},
    --     "t",
    --     function()
    --         awful.spawn(constants.apps.taskManger)
    --     end,
    --     {
    --         description = "open a task manager",
    --         group = "Apps"
    --     }
    -- ),
)
