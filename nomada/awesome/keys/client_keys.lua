local gears = require("gears")
local awful = require("awful")
local lain = require("lain")
local naughty = require("naughty")

local constants = require("lib/constants")

return gears.table.join(
    ---------------------------------------------------
    --  CLIENT ACTION
    ---------------------------------------------------
    awful.key(
        {
            constants.keys.mod
        },
        "q",
        function(c)
             c:kill() 
        end, 
        {
            description = "close",
            group = "client"
        }
    ), 
    ---------------------------------------------------
    --  CLIENT POSITION
    ---------------------------------------------------
    awful.key(
        {
            constants.keys.mod
        }, 
        "t",
        function(c) 
            c.ontop = not c.ontop 
        end,
        {
            description = "toggle keep on top",
            group = "client"
        }
    ), 
    awful.key(
        {
            constants.keys.mod
        }, 
        "n", 
        function(c)
            c.minimized = true
        end,
        {
            description = "minimize", 
            group = "client"
        }
    ),
    awful.key(
        {
            constants.keys.mod, 
            constants.keys.control
        },
        "space",
        function(c)
            awful.client.floating.toggle(c)
        end,
        {
            description = "toggle floating",
            group = "client"
        }
    ), 
    ---------------------------------------------------
    --  CLIENT SIZES
    ---------------------------------------------------
    awful.key(
        {
            constants.keys.mod
        }, 
        "f",
        function(c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end, 
        {
            description = "toggle fullscreen", 
            group = "client"
        }
    ),
    awful.key(
        {
            constants.keys.mod
        },
        "m",
        function(c)
            c.maximized = not c.maximized
            c:raise()
        end,
        {
            description = "(un)maximize", 
            group = "client"
        }
    ),
    awful.key(
        {
            constants.keys.mod, 
            constants.keys.control
        },
        "m", 
        function(c)
            c.maximized_vertical = not c.maximized_vertical
            c:raise()
        end,
        {
            description = "(un)maximize vertically", 
            group = "client"
        }
    ),
    awful.key(
        {
            constants.keys.mod, 
            constants.keys.shift
        },
        "m", 
        function(c)
            c.maximized_horizontal = not c.maximized_horizontal
            c:raise()
        end,
        {
            description = "(un)maximize horizontally", 
            group = "client"
        }
    )
    ---------------------------------------------------
    --  DISABLED
    ---------------------------------------------------
-- awful.key(
--     {
--         constants.keys.mod, 
--         constants.keys.control
--     }, 
--     "Return",
--     function(c)
--         c:swap(awful.client.getmaster()) 
--     end,
--     {
--         description = "move to master", 
--         group = "client"
--     }
-- ),
-- awful.key(
--     {
--         constants.keys.mod
--     }, 
--     "o",
--     function(c) 
--         c:move_to_screen() 
--     end,
--     {
--         description = "move to screen",
--         group = "client"
--     }
-- ),
)
