-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
local menubar = require("menubar")

-- local requirements
local global_keys = require("keys/global_keys")
local tag_keys = require("keys/tag_keys")
local client_keys = require("keys/client_keys")

return {
    global_keys = gears.table.join(global_keys,tag_keys),
    client_keys = client_keys,
}

-- local volume_widget = require("widgets/awesome-wm-widgets.volume-widget.volume")
-- local volume =require("widgets/volume")

-- volume.volume.widget:buttons(
--     awful.util.table.join(
--         awful.button(
--             {},
--             constants.buttons.left, 
--             function()
--                 awful.spawn("pavucontrol")
--             end
--         ),
--         awful.button(
--             {},
--             constants.buttons.middle, 
--             function()
--                 os.execute(
--                     string.format(
--                         "pactl set-sink-volume %s 100%%",
--                         volume.volume.device
--                     )
--                 )
--                 volume.volume.update()
--             end
--         ),
--         awful.button(
--             {},
--             constants.buttons.right, 
--             function()
--                 os.execute(
--                     string.format(
--                         "pactl set-sink-mute %s toggle",
--                         volume.volume.device
--                     )
--                 )
--                 volume.volume.update()
--             end
--         ),
--         awful.button(
--             {},
--             constants.buttons.scroll_up, 
--             function()
--                 os.execute(
--                     string.format(
--                         "pactl set-sink-volume %s +1%%",
--                         volume.volume.device
--                     )
--                 )
--                 volume.volume.update()
--             end
--         ),
--         awful.button(
--             {},
--             constants.buttons.scroll_down, 
--             function()
--                 os.execute(
--                     string.format(
--                         "pactl set-sink-volume %s -1%%",
--                         volume.volume.device
--                     )
--                 )
--                 volume.volume.update()
--             end
--         )
--     )
-- )