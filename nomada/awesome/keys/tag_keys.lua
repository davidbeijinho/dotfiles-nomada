local gears = require("gears")
local awful = require("awful")

local constants = require("lib/constants")

local tag_keys
-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    tag_keys = gears.table.join(
        tag_keys, 
        -- View tag only.
        awful.key(
            {
                constants.keys.mod
            }, 
            "#" .. i + 9,
            function()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then 
                    tag:view_only() 
                end
            end, 
            {
                description = "view tag #" .. i, 
                group = "tag"
            }
        ),
        -- Move client to tag.
        awful.key(
            {
                constants.keys.mod, 
                constants.keys.shift
            },
            "#" .. i + 9, 
            function()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then 
                        client.focus:move_to_tag(tag) 
                    end
                end
            end,
            {
                description = "move focused client to tag #" .. i, 
                group = "tag"
            }
        )
    )
end

return tag_keys