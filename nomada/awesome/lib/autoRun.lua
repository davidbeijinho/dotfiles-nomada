local awful = require("awful")

-- Autorun programs
autorun = true
autorunApps =
{
   -- "pamac-tray", -- TODO start as Sudo
   -- "nm-applet --sm-disable &",
   
   -- "xfce4-power-manager",
   -- "pulseaudio", 
   -- "light-locker",
   -- "ulauncher",
}
if autorun then
   for app = 1, #autorunApps do
       awful.util.spawn(autorunApps[app])
   end
end


-- {{{ Autostart windowless processes
local function run_once(cmd_arr)
   for _, cmd in ipairs(cmd_arr) do
       awful.spawn.with_shell(string.format("pgrep -u $USER -fx '%s' > /dev/null || (%s)", cmd, cmd))
   end
end

run_once({ 
   -- "xfce4-clipman",
   -- "unclutter -root" 
}) -- entries must be comma-separated