local awful = require("awful") -- Everything related to window managment

local buttons = {
    right = 3,
    left = 1,

    scroll_up = 4,
    scroll_down = 5,

    middle = 2
}

-- TODO USE THIS
-- local browser3 = "chromium -no-default-browser-check"
-- editor = os.getenv("EDITOR") or "nano"

local apps = {
    terminal = "terminator",
    -- editorgui
    editor = "code",
    browser = "firefox",
    fileManager = "pcmanfm",
    -- taskManger = "xfce4-taskmanager"
}

local keys = {
    mod = "Mod4", 
    alt = "Mod1", 
    control = "Control",
    shift = "Shift",
    tab = "Tab"
}

return {
    apps = apps,
    buttons = buttons,
    keys = keys
}
