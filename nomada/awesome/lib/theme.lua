-- Theme handling library
local beautiful = require("beautiful")

local themes = {
    "blackburn",       -- 1
    "copland",         -- 2
    "dremora",         -- 3
    "holo",            -- 4
    "multicolor",      -- 5
    "powerarrow",      -- 6
    "powerarrow-dark", -- 7
    "rainbow",         -- 8
    "steamburn",       -- 9
    "vertex",           -- 10
    "nomada"           -- 11
}

-- choose your theme here
local chosen_theme = themes[11]

-- create theme path
local theme_path = string.format("%s/.config/awesome/themes/%s/theme.lua", os.getenv("HOME"), chosen_theme)

-- Import theme
-- Themes define colours, icons, font and wallpapers.
beautiful.init(theme_path)
