local batteryarc_widget = require("awesome-wm-widgets.batteryarc-widget.batteryarc")

return batteryarc_widget({
    show_current_level = true,
    arc_thickness = 1,
})
