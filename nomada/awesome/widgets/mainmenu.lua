local freedesktop_menu = require("modules/freedesktop/menu")
local hotkeys_popup = require("awful.hotkeys_popup")

local constants = require("lib/constants")

return  freedesktop_menu.build({
    before = {
        { 
            "Awesome", {
                { "hotkeys", function() return false, hotkeys_popup.show_help end },
                { "restart", awesome.restart },
            } 
        },
    },
    after = {
        { "Terminal", constants.apps.terminal },
        { "Log out", function() awesome.quit() end },
        { "Sleep", "systemctl suspend" },
        { "Restart", "systemctl reboot" },
        { "Shutdown", "systemctl poweroff" },
    }
})
