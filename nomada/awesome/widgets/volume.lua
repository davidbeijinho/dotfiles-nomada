local wibox = require("wibox")
local awful = require("awful")
local gears = require("gears")
local constants = require("lib/constants")

local widget = wibox.widget {
        {
            max_value     = 1,
            value         = 0.33,
            widget        = wibox.widget.progressbar,
        },
        forced_height = 100,
        forced_width  = 20,
        direction     = 'east',
        layout        = wibox.container.rotate,
    }

local tooltip = awful.tooltip { }


tooltip:add_to_object(widget)

widget:connect_signal(
    'mouse::enter', 
    function()
        awful.spawn.easy_async(
            "pactl get-sink-volume @DEFAULT_SINK@", 
            function(stdout, stderr, reason, exit_code)
                tooltip.text =  gears.string.split(stdout, " ")[5]
            end
        )
    end
)

widget:connect_signal(
    "button::press", 
    function(_,_,_,button)
        if (button == constants.buttons.right or button == constants.buttons.left) then 
            awful.spawn("pavucontrol")
        end
    end
)

-- pactl set-sink-volume @DEFAULT_SINK@ 

-- volume.volume = lain.widget.pulse {
--     settings = function()
--         vlevel = volume_now.left .. "-" .. volume_now.right .. "% | " .. volume_now.device
--         if volume_now.muted == "yes" then
--             vlevel = vlevel .. " M"
--         end
--         widget:set_markup(lain.util.markup("#7493d2", vlevel))
--     end
-- }

return widget
