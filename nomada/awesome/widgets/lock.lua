local awful = require("awful")
local logout_menu_widget = require("awesome-wm-widgets.logout-menu-widget.logout-menu")

return logout_menu_widget{
    onlock = function() 
        awful.spawn.with_shell('slock') 
    end
}
