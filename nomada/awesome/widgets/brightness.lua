local brightness_widget = require("awesome-wm-widgets.brightness-widget.brightness")

return brightness_widget{
    type = 'icon_and_text',
    program = 'xbacklight',
    step = 2,        
}
