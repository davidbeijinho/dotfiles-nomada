# alias lh_checkout="~/Projects/lh-scripts/lh_checkout.sh"

# sdo() sudo zsh -c "$functions[$1]" "$@"

# runner="docker"
lhdevpath="/data/projects/WORK/lh-develop"
runner="podman"

# for podman
server_container_name=lh-develop_lh-server_1
# for docker
# server_container_name=lh-develop-lh-server-1

alias lhshell.s="
    $runner exec -i -t $server_container_name /bin/bash
"
alias lhshell.w="
    $runner exec -i -t lh-develop_lh-worker_1 /bin/bash
"

alias lhexec="
    $runner exec -t $server_container_name
"

alias lhdown="
    $runner-compose down
"

alias lhstop="
    lhdown
    rm -fr node_modules
    rm -fr ./client/node_modules
    rm -fr ./server/node_modules
    rm -fr ./lh-ui-toolkit/node_modules
"

alias lhclean="
    lhstop
    $runner volume rm lh-develop_mongo-data
    $runner volume rm lh-develop_db-data
    $runner volume rm lh-develop_lh-worker
    $runner volume rm lh-develop_lh-server
"

alias lhstart="
    lhclean
    lhup
"

alias lhinstall="$runner exec $server_container_name npm i"

alias lhui.install="npm --prefix ${lhdevpath}/lh-ui-toolkit install"

alias lhclient.install="npm --prefix ${lhdevpath}/client install"

alias storybook.run="npm --prefix ${lhdevpath}/client run storybook"
alias storybook.dev="
    lhui.install
    lhclient.install
    storybook.run
"

alias lhlint.ts.dev="npm --prefix ${lhdevpath}/client run lint:tsc"
alias lhlint.client.run="npm --prefix ${lhdevpath}/client run lint:fix"

alias lhlint.lhui.run="npm --prefix ${lhdevpath}/lh-ui-toolkit run lint:fix"

alias lhlint.lhui.dev="
    lhui.install
    lhlint.lhui.run
"

alias lhlint.client.dev="
    lhclient.install
    lhlint.client.run
"

alias lhlint.all.dev="
    lhlint.lhui.dev
    lhlint.client.dev
    lhlint.ts.dev
"

alias lhlint.all.run="
    lhlint.lhui.run
    lhlint.client.run
"

alias lhlint.all.install="
    lhlint.client.install
    lhlint.lhui.install
"

# alias lhdev="
# lhinstall &&
# lhui.run &
# lhcomponents.run
# "

# Lhdev() {
#     lhcomponents.dev
#     lhui.dev
# }

alias lhdev="
    {
        lhcomponents.dev
    } &
    {
        lhui.dev
    } &
"

Lhdevkill() {
    ps -ef | grep "styleguid" | grep -v grep | awk '{print $2}' | xargs kill -9
}

alias lhdevkill='Lhdevkill'

# alias lhup="
#     lhstop
#     lhinstall
#     $runner-compose up --build
# "

alias lhup.build="
    lhstop
    $runner-compose up --build
"

alias lhup="
    lhstop
    $runner-compose up
"

alias lhmigrate.up="
    lhmigrate.mysql.up
    lhmigrate.mongo.up
"
alias lhmigrate.down="
    lhmigrate.mysql.down
    lhmigrate.mongo.down
"
alias lhmigrate.mysql.up="
    $runner exec -e NODE_ENV=development $server_container_name npx knex --knexfile ./knexfile.sample.js migrate:latest
"
alias lhmigrate.mysql.down="
    $runner exec -e NODE_ENV=development $server_container_name npx knex --knexfile ./knexfile.sample.js migrate:rollback
"
alias lhmigrate.mongo.up="
    $runner exec -e NODE_ENV=development $server_container_name npx migrate-mongo up
"
alias lhmigrate.mongo.down="
    $runner exec -e NODE_ENV=development $server_container_name npx migrate-mongo down
"
# $runner exec -i -t lh-develop_mysql_1  mysql --user=root --password=123123  --execute=\"ALTER USER 'laserhub'@'%' IDENTIFIED WITH mysql_native_password BY '123123';\"
#    $runner exec -i -t lh-develop_mysql_1  mysql --user=\"root\" --password=\"123123\"  --execute=\"ALTER USER 'laserhub'@'%' IDENTIFIED WITH mysql_native_password BY '123123';\"
alias lhseed="
    $runner exec -e NODE_ENV=development $server_container_name npx knex --knexfile ./knexfile.sample.js seed:run
"
# lhmigrate


alias VolumePrune="$runner volume prune"



alias lhfront.run="npm run start --prefix ${lhdevpath}/client"
alias lhfront-dev.run="LH_SERVER_ADDRESS='https://dev.laserhub.com' lhfront.run"
alias lhfront-slt.run="LH_SERVER_ADDRESS='https://mercury-sandbox.laserhub.com' lhfront.run"
alias lhfront-dev.dev="lhui.install && lhclient.install && lhfront-dev.run"
alias lhfront-slt.dev="lhui.install && lhclient.install && lhfront-slt.run"
