export PATH="$PATH:$HOME/bin:/data/DataBin"


alias k9s="podman run --rm -it -v /home/nomada/.kube/config:/root/.kube/config quay.io/derailed/k9s"
alias k9s-mini="podman run --rm -it -v /home/nomada/.kube/mini-nomada:/root/.kube/config quay.io/derailed/k9s"
alias k9s-maxi="podman run --rm -it -v /home/nomada/.kube/maxi-nomada:/root/.kube/config quay.io/derailed/k9s"
alias k9s-panda="podman run --rm -it -v /home/nomada/.kube/panda-nomada:/root/.kube/config quay.io/derailed/k9s"
alias k9s-contabo="podman run --rm -it -v /home/nomada/.kube/contabo-nomada:/root/.kube/config quay.io/derailed/k9s"
# alias k9s-mood="podman run --rm -it -v /home/nomada/.kube/mood-nomada:/root/.kube/config quay.io/derailed/k9s"
# alias k9s-urso="podman run --rm -it -v /home/nomada/.kube/urso-nomada:/root/.kube/config quay.io/derailed/k9s"

export KUBECONFIG=~/.kube/config:~/.kube/mini-nomada:~/.kube/panda-nomada:~/.kube/contabo-nomada:~/.kube/maxi-nomada
# ~/.kube/mood-nomada:
# alias screenDocked="wlr-randr --output DP-4 --mode 3440x1440 px, 75.050003 Hz --pos 0,0 && wlr-randr --output eDP-1 --pos 3445,360"
# alias screenDocked1="wlr-randr --output DP-5 --mode 3440x1440 px, 75.050003 Hz --pos 0,0 && wlr-randr --output eDP-1 --pos 3445,360"

alias brain-update="git -C /home/nomada/Sync/brain-nomada stash --all  && git -C /home/nomada/Sync/brain-nomada pull && git -C /home/nomada/Sync/brain-nomada stash apply"
