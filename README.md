# dotfiles-nomada

My dotfiles repository

## gitmodules for awesomewm

```
	[submodule "files/config/awesome/modules/freedesktop"]
		path = files/config/awesome/modules/freedesktop
		url = https://github.com/lcpz/awesome-freedesktop
	[submodule "files/config/awesome/freedesktop"]
		path = files/config/awesome/freedesktop
		url = https://github.com/lcpz/awesome-freedesktop	
	[submodule "files/config/awesome/lain"]
		path = files/config/awesome/lain
		url = https://github.com/lcpz/lain
		ignore = dirty
	[submodule "lain.wiki"]
		path = files/config/awesome/lain/wiki
		url = https://github.com/lcpz/lain.wiki.git
	[submodule "files/config/awesome/awesome-wm-widgets"]
		path = files/config/awesome/awesome-wm-widgets
		url = https://github.com/streetturtle/awesome-wm-widgets
	[submodule "files/config/awesome/modules/arc-icon-theme"]
		path = files/config/awesome/modules/arc-icon-theme
		url = https://github.com/horst3180/arc-icon-theme
```


i3

i3-msg -t get_tree